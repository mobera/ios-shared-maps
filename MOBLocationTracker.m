//
//  MOBLocationTracker.m
//  MapViewer
//
//  Created by Jakub Dubrovsky on 30/06/14.
//  Copyright (c) 2014 Mobera. All rights reserved.
//

#import "MOBLocationTracker.h"

@interface MOBLocationTracker ()

@end


@implementation MOBLocationTracker
static MOBLocationTracker * _locationTracker;



+ (instancetype)sharedTracker
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        _locationTracker = [[MOBLocationTracker alloc] init];
    });
    return _locationTracker;
}

#pragma mark -

- (id)init{
    if (self = [super init]) {
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
        [_locationManager setDistanceFilter:20.0];
        [_locationManager setActivityType:CLActivityTypeFitness];
        
        _path = @[].mutableCopy;
    }
    return self;
}

- (void)start
{
    [self.path removeAllObjects];
    [self.locationManager startUpdatingLocation];
    self.isUpdating = YES;
}

- (void)stop
{
    [self.locationManager stopUpdatingLocation];
    self.isUpdating = NO;
    [self.path removeAllObjects];
}

- (void)pause
{
    if (self.isUpdating) {
        [self.locationManager stopUpdatingLocation];
    }else{
        [self.locationManager startUpdatingLocation];
    }
    self.isUpdating = !self.isUpdating;
}

- (NSString *)geojson
{
    NSMutableArray *geojsonPath = @[].mutableCopy;
    
    for (CLLocation *location in self.path) {
        NSArray *t = @[@(location.coordinate.longitude),@(location.coordinate.latitude)];
        [geojsonPath addObject:t];
    }
    
    NSDictionary *geojson = @{
                              @"type" : @"FeatureCollection",
                              @"features" : @[@{
                                                  @"type" : @"Feature",
                                                  @"properties" : @{},
                                                  @"geometry" : @{
                                                          @"type" : @"LineString",
                                                          @"coordinates" : geojsonPath
                                                          }
                                                  }
                                              ]
                              };
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:geojson
                                                       options:0
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString ;
}

#pragma mark CLLocatioManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self.path addObjectsFromArray:locations];
}
@end
