//
//  MOBTileOverlay.m
//  MapViewer
//
//  Created by Jakub Dubrovsky on 29/06/14.
//  Copyright (c) 2014 Mobera. All rights reserved.
//

#import "MOBTileOverlay.h"

@interface MOBTileOverlay ()

@end

@implementation MOBTileOverlay

NSUInteger long2tilex(double lon, int z)
{
	return (NSUInteger)(floor((lon + 180.0) / 360.0 * pow(2.0, z)));
}

NSUInteger lat2tiley(double lat, int z)
{
	return (NSUInteger)(floor((1.0 - log( tan(lat * M_PI/180.0) + 1.0 / cos(lat * M_PI/180.0)) / M_PI) / 2.0 * pow(2.0, z)));
}

/*
 
 @"url" : @"http://tile.openstreetmap.org/{z}/{x}/{y}.png",
 @"title" : @"OSM",
 @"id" : @"osm1"
 },
 */


- (id)initWithURLTemplate:(NSString *)URLTemplate withTitle:(NSString *)title withId:(NSString *)xid
{
    if (self = [self initWithURLTemplate:URLTemplate]) {
        _o_source = URLTemplate;
        _o_title = title;
        _o_id = xid;
    }
    return self;
}
#pragma mark - cache

/**
 *  Download tiles in the region
 *
 *  @param region  <#region description#>
 *  @param zoomMin <#zoomMin description#>
 *  @param zoomMax <#zoomMax description#>
 *
 *  @return <#return value description#>
 */
- (void) cacheDownloadRegion: (MKCoordinateRegion) region zoomMin:(NSUInteger) zoomMin zoomMax: (NSUInteger) zoomMax
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^{
        
        CLLocationCoordinate2D center = region.center;
        CLLocationCoordinate2D northWestCorner, southEastCorner;
        northWestCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / 2.0);
        northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / 2.0);
        southEastCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / 2.0);
        southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / 2.0);
        
        NSUInteger tilesAll =0;
        for (NSUInteger zoom=1; zoom<zoomMax; zoom++) {
            NSUInteger x1 = long2tilex(northWestCorner.longitude, zoom);
            NSUInteger x2 = long2tilex(southEastCorner.longitude, zoom);
            NSUInteger y1 = lat2tiley(northWestCorner.latitude, zoom);
            NSUInteger y2 = lat2tiley(southEastCorner.latitude, zoom);
            
            tilesAll += (x2-x1) * (y2-y1) * zoomMax;
        }
        
        NSUInteger tilesDone = 0;
        
        for (NSUInteger zoom=zoomMin; zoom<=zoomMax; zoom++) {
            NSUInteger x1 = long2tilex(northWestCorner.longitude, zoom);
            NSUInteger x2 = long2tilex(southEastCorner.longitude, zoom);
            NSUInteger y1 = lat2tiley(northWestCorner.latitude, zoom);
            NSUInteger y2 = lat2tiley(southEastCorner.latitude, zoom);
            
            //  NSLog(@"zoom %i x: %i - %i = %i y: %i - %i = %i",zoom,x1,x2,x1-x2,y1,y2,y1-y2);
            
            for (NSUInteger y = y1; y <= y2; y++) {
                for (NSUInteger x = x1; x <= x2; x++) {
                    tilesDone++;
                    
                    MKTileOverlayPath tileOverlayPath;
                    tileOverlayPath.z = zoom;
                    tileOverlayPath.x = x;
                    tileOverlayPath.y = y;
                    
                    
                    NSString *cachePath = [self cacheMapPath:tileOverlayPath];
                    
                    NSData *cachedData = [NSData dataWithContentsOfFile:cachePath];
                    if (cachedData) {
                        continue;
                    }
                    
                    
                    NSData *data = [NSData dataWithContentsOfURL:[self urlTileAtX:x atY:y atZoom:zoom]];
                    if (!data) {
                        NSLog(@"err data ");
                        //TODO: ddd
                    }
                    
                    NSError *error;
                    [data writeToFile:cachePath options:NSDataWritingAtomic error:&error];
                    
                    if (error) {
                    //    return ;
                        //TODO:ddd
                    }
                }
            }
        }
    });
}







- (NSString *)cacheMapPath
{
    NSString *documentsDirectory = NSTemporaryDirectory();
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/map-%@", self.o_id]];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    if (error) {
        NSLog(@"error %@", error);
    }
    
    return dataPath;
}
- (NSString *) cacheMapPath:(MKTileOverlayPath)path
{
    return [[self cacheMapPath] stringByAppendingString:[NSString stringWithFormat:@"/%d-%d-%d.png", path.z, path.x, path.y]];
}

/**
 *  Computed cached size for the layer
 *
 *  @return <#return value description#>
 */
- (void)cacheSize
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^{
        
        NSString *folderPath = [self cacheMapPath];
        
        NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
        NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
        NSString *fileNames;
        unsigned long long int fileSize = 0;
        //fileSize = 0;
        
        while (fileNames = [filesEnumerator nextObject]) {
            NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileNames] error:nil];
            fileSize += [fileDictionary fileSize];
        }
        NSLog(@"filesize==%llu",fileSize);
        
        fileSize = ((float)fileSize) / (1024*1024);
        
        //TODO:
       
    });

}
/**
 *  Remove all cached tiles
 *
 *  @return <#return value description#>
 */

- (void)cacheReset
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^{
        NSString *folderPath = [self cacheMapPath];
        
        NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
        NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
        NSString *fileNames;
        
        while (fileNames = [filesEnumerator nextObject]) {
            NSString *fileName = [folderPath stringByAppendingString:[NSString stringWithFormat: @"/%@", fileNames]];
            if (![[NSFileManager defaultManager] removeItemAtPath:fileName error:NULL]) {
                NSLog(@"[Error] %@ ", fileName);
            }
        }
        //TODO:
        
    });
}

#pragma mark -
- (NSURL *) urlTileAtX: (NSUInteger) x atY:(NSUInteger) y atZoom:(NSUInteger) zoom
{
    NSString *string = [[[self.o_source stringByReplacingOccurrencesOfString:@"{x}" withString:[NSString stringWithFormat:@"%i", x]] stringByReplacingOccurrencesOfString:@"{y}" withString:[NSString stringWithFormat:@"%i", y]] stringByReplacingOccurrencesOfString:@"{z}" withString:[NSString stringWithFormat:@"%i", zoom]];
    return [NSURL URLWithString:string];
}


#pragma mark - MOBManagedObjectSerialization
- (void)managedObjectPopulate:(NSDictionary *)data
{
    _o_source = data[@"url"];
    _o_title = data[@"title"];
    _o_id = data[@"id"];
}


#pragma mark - TileOverlay
- (void)loadTileAtPath:(MKTileOverlayPath)path result:(void (^)(NSData *data, NSError *error))result
{
    if (!result) {
        return;
    }

    __block NSString *cachePath = [self cacheMapPath:path];
    
    NSData *cachedData = [NSData dataWithContentsOfFile:cachePath];
    if (cachedData) {
        result(cachedData, nil);
    }else{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[self URLForTilePath:path]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            NSError *error;
            [data writeToFile:cachePath options:NSDataWritingAtomic error:&error];
            
            result(data, connectionError);
        }];
    }
}
@end
