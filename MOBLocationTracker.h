//
//  MOBLocationTracker.h
//  MapViewer
//
//  Created by Jakub Dubrovsky on 30/06/14.
//  Copyright (c) 2014 Mobera. All rights reserved.
//

@import CoreLocation;

@interface MOBLocationTracker : NSObject<CLLocationManagerDelegate>

+ (instancetype) sharedTracker;


@property(nonatomic,readonly) CLLocationManager *locationManager;
@property(nonatomic, readonly) NSMutableArray *path;

@property(nonatomic) BOOL isUpdating;

-(void) start;
-(void) pause;
-(void) stop;
-(NSString *) geojson;
@end
