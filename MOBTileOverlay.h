//
//  MOBTileOverlay.h
//  MapViewer
//
//  Created by Jakub Dubrovsky on 29/06/14.
//  Copyright (c) 2014 Mobera. All rights reserved.
//


#import <MapKit/MapKit.h>

@interface MOBTileOverlay : MKTileOverlay

/**
 *  Title of the overlay
 */
@property (nonatomic, readonly) NSString * o_title;

/**
 *  Example http://tile.openstreetmap.org/{z}/{x}/{y}.png
 */
@property (nonatomic, readonly) NSString * o_source;
/**
 *  Tile id, for caching and
 */
@property (nonatomic, readonly) NSString * o_id;



- (id)initWithURLTemplate:(NSString *)URLTemplate withTitle: (NSString *) title withId: (NSString *) xid;


- (NSURL *) urlTileAtX: (NSUInteger) x atY:(NSUInteger) y atZoom:(NSUInteger) zoom;

- (void) cacheDownloadRegion: (MKCoordinateRegion) region zoomMin:(NSUInteger) zoomMin zoomMax: (NSUInteger) zoomMax;
- (void) cacheReset;
//- (void) cacheStop;
//- (void) cacheStart;
- (void) cacheSize;
- (NSString *) cacheMapPath;
- (NSString *) cacheMapPath:(MKTileOverlayPath)path;


@end
